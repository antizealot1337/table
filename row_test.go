package table

import (
	"bytes"
	"testing"
)

func TestEmptyRow(t *testing.T) {
	// Create an empty row
	r := EmptyRow()

	// Make sure the row isn't nil
	if r == nil {
		t.Fatal("Row was nil")
	} //if

	// Check the number of cols
	if len(r.cols) != 0 {
		t.Error("Row has unexpected cols")
	} //if
} //func

func TestNewRow(t *testing.T) {
	// Create a row with an initial capacity
	r := NewRow(10)

	// Make sure the row isn't nil
	if r == nil {
		t.Fatal("Row was nil")
	} //if

	// Check the number of cols
	if len(r.cols) != 0 {
		t.Error("Row has unexpected cols")
	} //if

	// Check the capacity
	if expected, actual := 10, cap(r.cols); actual != expected {
		t.Errorf("Expected cap %d but was %d", expected, actual)
	} //if
} //func

func TestRowCols(t *testing.T) {
	// Create a row
	var r Row

	// Check the number of cols
	if expected, actual := 0, r.Cols(); actual != expected {
		t.Errorf("Expected %d cols but was %d", expected, actual)
	} //if

	// Create a Cell
	c := AlignLeft("")

	// Add a column
	r.cols = append(r.cols, &c)

	// Check the number of cols
	if expected, actual := 1, r.Cols(); actual != expected {
		t.Errorf("Expected %d cols but was %d", expected, actual)
	} //if
} //func

func TestRowColLen(t *testing.T) {
	// Create some cells
	a := AlignLeft("A")

	// Create a row
	r := Row{
		cols: []Cell{&a},
	} //struct

	// Check a negative index
	if expected, actual := -1, r.ColLen(-1); actual != expected {
		t.Errorf("Expected len %d but was %d", expected, actual)
	} //if

	// Check the column length for the first index
	if expected, actual := 1, r.ColLen(0); actual != expected {
		t.Errorf("Expected len %d but was %d", expected, actual)
	} //if

	// Check the column length for the second index
	if expected, actual := -1, r.ColLen(1); actual != expected {
		t.Errorf("Expected len %d but was %d", expected, actual)
	} //if

} //func

func TestRowAdjust(t *testing.T) {
} //func

func TestRowAdd(t *testing.T) {
	// Create a row
	var r Row

	// Create a Cell
	c := AlignLeft("column")

	// Add a column
	r.Add(&c)

	// Check the number of cols
	if expected, actual := 1, len(r.cols); actual != expected {
		t.Fatalf("Expected %d cols but was %d", expected, actual)
	} //if

	// Check the value of the first column
	if expected, actual := "column", r.cols[0].String(); actual != expected {
		t.Errorf(`Expected col 1 "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestRowWriteTo(t *testing.T) {
	// Create some Cells
	var a, b, c AlignLeft = "A", "B", "C"

	// Create a row
	r := Row{
		cols: []Cell{
			&a, &b, &c,
		}, //slice
	} //struct

	buff := bytes.NewBuffer(nil)

	_, err := r.WriteTo(buff)

	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check the string method
	if expected, actual := "A B C", buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestRowString(t *testing.T) {
	// Create some Cells
	var a, b, c AlignLeft = "A", "B", "C"

	// Create a row
	r := Row{
		cols: []Cell{
			&a, &b, &c,
		}, //slice
	} //struct

	// Check the string method
	if expected, actual := "A B C", r.String(); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func
