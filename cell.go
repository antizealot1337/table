package table

// Cell is the data in the Row and thus Table.
type Cell interface {
	// Pad called when the Table wishes to adjust the Cell size.
	Pad(size int)

	// Len is called when a Table is calculating the Cell sizes before padding.
	Len() int

	// String will return a string repesentation of the Cell.
	String() string
} //interface
