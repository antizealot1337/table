# Table

A fairly simple and somewhat flexible command line table implementation. Some
features like alignment for strings is built-in. Complex types can override
`Cell` for more control over output and layout like adding ANSI colors ;)

# License
Licensed under the terms of the MIT license. See LICENSE for more information.
