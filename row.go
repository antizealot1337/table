package table

import (
	"io"
	"strings"
)

// Row is a single row in a Table and contains columns of Cells.
type Row struct {
	cols []Cell
} //struct

// EmptyRow returns a Row with no default column size.
func EmptyRow() *Row {
	return &Row{}
} //func

// NewRow returns a Row with the provided default column size. Use this when
// the number of columns is known when the Row is constructed.
func NewRow(size int) *Row {
	// Check for a bad size
	if size <= 0 {
		// Just return an empty row
		return EmptyRow()
	} //if

	return &Row{
		cols: make([]Cell, 0, size),
	}
} //func

// Cols returns the number of columns in the Row.
func (r *Row) Cols() int {
	return len(r.cols)
} //func

// ColLen returns the length of the Cell at the provided index. If the index is
// invalid -1 is returned.
func (r *Row) ColLen(index int) int {
	// Check to make sure the index is in bounds
	if index >= len(r.cols) || index < 0 {
		return -1 // Return -1 to indicate the index was bad
	} //if

	// Return the length of the column
	return r.cols[index].Len()
} //func

// Adjust will loop through all the columns and attempt to pad the Cells using
// the provided lengths. The number of lengths is expected to match the number
// of columns/Cells in the Row
func (r *Row) Adjust(lens []int) {
	// Loop through columns
	for i, col := range r.cols {
		// Check if this is the longest column
		if col.Len() == lens[i] {
			continue
		} //if

		// Tell the cell to pad itself
		col.Pad(lens[i])
	} //for
} //func

// Add a Cell.
func (r *Row) Add(c Cell) {
	r.cols = append(r.cols, c)
} //func

// WriteTo will write the Row to the provided Writer.
func (r Row) WriteTo(w io.Writer) (size int64, err error) {
	// Loop through the columns
	for i, col := range r.cols {
		// Write the column string to the writer
		var s int
		s, err = io.WriteString(w, col.String())

		// Add to the size
		size += int64(s)

		// Check for an error
		if err != nil {
			return
		} //if

		// Check if a space should be written
		if i != len(r.cols)-1 {
			// Write the space
			s, err = io.WriteString(w, " ")

			// Add to the size
			size += int64(s)

			// Check for an error
			if err != nil {
				return
			} //if
		} //if
	} //for

	return
} //func

// String will return a string of the Row.
func (r *Row) String() string {
	// Create a slice
	s := make([]string, 0, len(r.cols))

	// Loop through the cols
	for _, col := range r.cols {
		s = append(s, col.String())
	} //for
	return strings.Join(s, " ")
} //func
