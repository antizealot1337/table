package table

// AlignLeft is a string based Cell that aligns left (pads to the right).
type AlignLeft string

// LeftAlign creates a new AlignLeft.
func LeftAlign(s string) *AlignLeft {
	a := AlignLeft(s)
	return &a
} //func

// Pad instructs the AlignLeft to add spaces to the right.
func (a *AlignLeft) Pad(l int) {
	// Convert the string to a slice
	rs := []rune(*a)

	// Add spaces to the end
	for len(rs) < l {
		rs = append(rs, ' ')
	} //for

	// Return the slice as a string
	*a = AlignLeft(rs)
} //func

// Len returns the length of the underlying string.
func (a *AlignLeft) Len() int {
	return len(string(*a))
} //func

// String will return the underlying string.
func (a *AlignLeft) String() string {
	return string(*a)
} //func

// AlignRight is a string based Cell that aligns right (pads to the left).
type AlignRight string

// RightAlign creates a new AlignRight.
func RightAlign(s string) *AlignRight {
	a := AlignRight(s)
	return &a
} //func

// Pad instructs the AlignRight to add spaces to the left.
func (a *AlignRight) Pad(l int) {
	// Create a slice
	rs := make([]rune, 0, l)

	// Set the spaces
	for i := 0; i < l-len(*a); i++ {
		rs = append(rs, ' ')
	} //for

	// Add the existing to the end
	rs = append(rs, []rune(*a)...)

	// Set the string
	*a = AlignRight(rs)
} //func

// Len returns the length of the underlying string.
func (a *AlignRight) Len() int {
	return len(string(*a))
} //func

// String will return the underlying string.
func (a *AlignRight) String() string {
	return string(*a)
} //func

// AlignCenter is a string based Cell that aligns to the center.
type AlignCenter string

// CenterAlign creates a new AlignCenter.
func CenterAlign(s string) *AlignCenter {
	a := AlignCenter(s)
	return &a
} //func

// Pad instructs the AlignCenter to add spaces to pad both the start and end of
// the string. If the string needs to pad an odd number the extra space goes to
// the end of the string.
func (a *AlignCenter) Pad(l int) {
	// Calculate the total padding amount
	total := l - a.Len()

	// The padding amount on eact side
	padding := total / 2

	// Create a slice to hold the new string
	rs := make([]rune, l)

	// Add the padding to start of the string
	for i := 0; i < padding; i++ {
		rs[i] = ' '
	} //for

	// Add the string
	for i, c := range []rune(*a) {
		rs[i+padding] = c
	} //for

	// Add the padding to end of the string
	for i := 0; i < padding; i++ {
		rs[i+padding+a.Len()] = ' '
	} //for

	// Check if it's even or odd
	if total%2 == 1 {
		rs[len(rs)-1] = ' '
	} //if

	// Set to the new string
	*a = AlignCenter(rs)
} //func

// Len returns the length of the underlying string.
func (a *AlignCenter) Len() int {
	return len(string(*a))
} //func

// String will return the underlying string.
func (a *AlignCenter) String() string {
	return string(*a)
} //func
