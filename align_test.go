package table

import "testing"

// Make sure AlignLeft implements Cell
var _ Cell = (*AlignLeft)(nil)

// Make sure AlignRight implements Cell
var _ Cell = (*AlignRight)(nil)

// Make sure AlignCenter implements Cell
var _ Cell = (*AlignCenter)(nil)

func TestLeftAlign(t *testing.T) {
	// Create an AlignLeft
	a := LeftAlign("A")

	// Make sure a isn't nil
	if a == nil {
		t.Fatal("Did not expect nil AlignLeft")
	} //if

	if expected, actual := "A", string(*a); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestAlignLeftPad(t *testing.T) {
	// Create an AlignLeft
	var a AlignLeft = "a"

	// Pad the AlignLeft
	a.Pad(3)

	// Convert to a string
	s := string(a)

	// Check the length
	if expected, actual := 3, len(s); actual != expected {
		t.Errorf("Expected len %d but was %d", expected, actual)
	} //if

	// Check the value
	if expected, actual := "a  ", s; actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestAlignLeftLen(t *testing.T) {
	// Create an AlignLeft
	var a AlignLeft = "a"

	// Check the length
	if expected, actual := 1, a.Len(); actual != expected {
		t.Errorf("Expected the len to be %d but was %d", expected,
			actual)
	} //if
} //func

func TestAlignLeftString(t *testing.T) {
	// Create an AlignLeft
	var a AlignLeft = "a"

	// Check stringer
	if expected, actual := "a", a.String(); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestRightAlign(t *testing.T) {
	// Create an AlignRight
	a := RightAlign("A")

	// Make sure a isn't nil
	if a == nil {
		t.Fatal("Did not expect nil AlignRight")
	} //if

	if expected, actual := "A", string(*a); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestAlignRightPad(t *testing.T) {
	// Create an AlignRight
	var a AlignRight = "a"

	// Pad the AlignRight
	a.Pad(3)

	// Convert to a string
	s := string(a)

	// Check the length
	if expected, actual := 3, len(s); actual != expected {
		t.Errorf("Expected len %d but was %d", expected, actual)
	} //if

	// Check the value
	if expected, actual := "  a", s; actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if

} //func

func TestAlignRightLen(t *testing.T) {
	// Create an AlignRight
	var a AlignRight = "a"

	// Check the length
	if expected, actual := 1, a.Len(); actual != expected {
		t.Errorf("Expected the len to be %d but was %d", expected,
			actual)
	} //if
} //func

func TestAlignRightString(t *testing.T) {
	// Create an AlignRight
	var a AlignRight = "a"

	// Check stringer
	if expected, actual := "a", a.String(); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestCenterAlign(t *testing.T) {
	// Create an AlignCenter
	a := CenterAlign("A")

	// Make sure a isn't nil
	if a == nil {
		t.Fatal("Did not expect nil AlignCenter")
	} //if

	if expected, actual := "A", string(*a); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestAlignCenterPad(t *testing.T) {
	// Create an AlignCenter
	var a AlignCenter = "a"

	// Pad the AlignCenter
	a.Pad(3)

	// Convert to a string
	s := string(a)

	// Check the length
	if expected, actual := 3, len(s); actual != expected {
		t.Errorf("Expected len %d but was %d", expected, actual)
	} //if

	// Check the value
	if expected, actual := " a ", s; actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if

	// Set AlignCenter again
	a = AlignCenter("a")

	// Pad to an even number
	a.Pad(4)

	// Convert to a string
	s = string(a)

	// Check the value
	if expected, actual := " a  ", s; actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if

	// Create an AlignCenter with an even number of chars
	a = AlignCenter("aa")

	// Pad the AlignCenter
	a.Pad(3)

	// Convert to a string
	s = string(a)

	// Check the value
	if expected, actual := "aa ", s; actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if

	// Create an AlignCenter with an even number of chars
	a = AlignCenter("aa")

	// Pad the AlignCenter
	a.Pad(4)

	// Convert to a string
	s = string(a)

	// Check the value
	if expected, actual := " aa ", s; actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if

} //func

func TestAlignCenterLen(t *testing.T) {
	// Create an AlignCenter
	var a AlignCenter = "a"

	// Check the length
	if expected, actual := 1, a.Len(); actual != expected {
		t.Errorf("Expected the len to be %d but was %d", expected,
			actual)
	} //if
} //func

func TestAlignCenterString(t *testing.T) {
	// Create an AlignCenter
	var a AlignCenter = "a"

	// Check stringer
	if expected, actual := "a", a.String(); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func
