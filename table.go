package table

import (
	"bytes"
	"io"
)

// Table contains all the Rows (and Cells).
type Table struct {
	rows []*Row
} //struct

// Empty creates a Table with no default rows size.
func Empty() *Table {
	return &Table{}
} //func

// New creates a new Table with the provided default row size. Use this when
// the number of rows is knows when the Table is constructed.
func New(size int) *Table {
	// Check for a bad size
	if size <= 0 {
		// Just return an empty table
		return Empty()
	} //if
	return &Table{
		rows: make([]*Row, 0, size),
	}
} //func

// Add a Row.
func (t *Table) Add(r *Row) {
	t.rows = append(t.rows, r)
} //func

// NewRow adds a new Row with the provided Cells.
func (t *Table) NewRow(cells ...Cell) {
	// Create and add a new Row
	t.Add(&Row{
		cols: cells,
	})
} //func

// Adjust will calculate the max column(Cell) length and the instruct all Cells
// to pad to the longest column.
func (t *Table) Adjust() {
	// Get the column lenths
	cls := t.columnLens()

	// Loop through the rows
	for _, row := range t.rows {
		// Adjust the row
		row.Adjust(cls)
	} //if
} //func

func (t *Table) columnLens() []int {
	// Get the longest row length
	longest := t.longestLen()

	// Check if the longest indicates there's no data
	if longest < 1 {
		// Nothing to do
		return nil
	} //if

	// Create a slice to house the max column length
	lens := make([]int, longest)

	// Loop through the column indexes
	for i := 0; i < longest; i++ {
		// Loop through the rows
		for _, row := range t.rows {
			// Get the length
			cl := row.ColLen(i)

			// Check if the column length is -1
			if cl == -1 {
				continue
			} //if

			// Check if this length is greater than the one stored
			if lens[i] < cl {
				lens[i] = cl
			} //if
		} //for
	} //for

	// Return the column lengths
	return lens
} //func

func (t *Table) longestLen() int {
	// Set the initial longest row value
	longest := -1

	// Loop through the rows
	for _, row := range t.rows {
		// Check if the current row's length (column count) is longer
		// than the longest encountered so far
		if l := row.Cols(); l > longest {
			longest = l
		} //if
	} //for

	// Return the longest row
	return longest
} //func

// WriteTo will write the Table to the provided Writer.
func (t *Table) WriteTo(w io.Writer) (size int64, err error) {
	// Loop through the rows
	for _, row := range t.rows {
		var s1 int64
		s1, err = row.WriteTo(w)

		// Add to the size
		size += s1

		// Check for an error
		if err != nil {
			return
		} //if

		// Add a newline
		var s2 int
		s2, err = io.WriteString(w, "\n")

		// Add to the size
		size += int64(s2)

		// Check for an error
		if err != nil {
			return
		} //if
	} //for
	return
} //func

// String will return a string of the Table.
func (t *Table) String() string {
	var buff bytes.Buffer
	t.WriteTo(&buff)
	return buff.String()
} //func
