package table

import (
	"bytes"
	"testing"
)

func TestEmpty(t *testing.T) {
	// Create an empty table
	tbl := Empty()

	// Make sure table isn't nil
	if tbl == nil {
		t.Fatal("Table should not be nil")
	} //if

	// Check the number of the rows
	if expected, actual := 0, len(tbl.rows); actual != expected {
		t.Errorf("Expected %d rows but was %d", expected, actual)
	} //if
} //func

func TestNew(t *testing.T) {
	// Create a new table
	tbl := New(10)

	// Make sure table isn't nil
	if t == nil {
		t.Fatal("Table should not be nil")
	} //if

	// Check the number of the rows
	if expected, actual := 0, len(tbl.rows); actual != expected {
		t.Errorf("Expected %d rows but was %d", expected, actual)
	} //if

	// Check the capacity of rows
	if expected, actual := 10, cap(tbl.rows); actual != expected {
		t.Errorf("Expected %d capacity but was %d", expected, actual)
	} //if
} //func

func TestTableAdd(t *testing.T) {
	// Create a table
	var tbl Table

	// Check the number of rows
	if expected, actual := 0, len(tbl.rows); actual != expected {
		t.Errorf("Expected %d rows but was %d", expected, actual)
	} //if

	// Add a row
	tbl.Add(&Row{})

	// Check the number of rows
	if expected, actual := 1, len(tbl.rows); actual != expected {
		t.Errorf("Expected %d rows but was %d", expected, actual)
	} //if
} //func

func TestTableNewRow(t *testing.T) {
	// Create a table
	var tbl Table

	// Add a row with nil values
	tbl.NewRow(nil, nil, nil)

	// Check the number of rows
	if expected, actual := 1, len(tbl.rows); actual != expected {
		t.Fatalf("Expected %d rows but was %d", expected, actual)
	} //if

	// Check the number of cells
	if expected, actual := 3, len(tbl.rows[0].cols); actual != expected {
		t.Errorf("Expected %d cells but was %d", expected, actual)
	} //if
} //func

func TestTableAdjust(t *testing.T) {
	// Create some cells
	var a, ab, b, bcd, cd, d AlignLeft = "A", "AB", "B", "BCD", "CD", "D"

	// Create a table
	tbl := Table{
		rows: []*Row{
			&Row{cols: []Cell{&ab}},
			&Row{cols: []Cell{&a, &bcd, &d}},
			&Row{cols: []Cell{&a, &b, &cd}},
		}, //slice
	} //struct

	// Adjust the table
	tbl.Adjust()

	// Check the rows
	for _, row := range tbl.rows {
		for i, col := range row.cols {
			var expected int

			// Get the length
			actual := col.Len()

			// Set expected based on the index
			switch i {
			case 0:
				expected = 2
			case 1:
				expected = 3
			case 2:
				expected = 2
			default:
				t.Fatal("bad index:", i)
			} //switch

			// Check the length
			if actual != expected {
				t.Errorf("Expected len %d but was %d for idx %d",
					expected, actual, i)
			} //if
		} //for
	} //for
} //func

func TestTableCollumnLens(t *testing.T) {
	// Create some cells
	var a, ab, b, bcd, cd, d AlignLeft = "A", "AB", "B", "BCD", "CD", "D"

	// Create a table
	tbl := Table{
		rows: []*Row{
			&Row{cols: []Cell{&ab}},
			&Row{cols: []Cell{&a, &bcd, &d}},
			&Row{cols: []Cell{&a, &b, &cd}},
		}, //slice
	} //struct

	// Get the column lengtsh
	cls := tbl.columnLens()

	// Check the length
	if expected, actual := len(tbl.rows), len(cls); actual != expected {
		t.Fatalf("Expected %d column lens but was %d", expected,
			actual)
	} //if

	// Check the first length
	if expected, actual := 2, cls[0]; actual != expected {
		t.Errorf("Expected first %d but was %d", expected, actual)
	} //if

	// Check the second length
	if expected, actual := 3, cls[1]; actual != expected {
		t.Errorf("Expected second %d but was %d", expected, actual)
	} //if

	// Check the third length
	if expected, actual := 2, cls[2]; actual != expected {
		t.Errorf("Expected third %d but was %d", expected, actual)
	} //if
} //func

func TestTableLongestLen(t *testing.T) {
	// Create a table
	var tbl Table

	// Check an empty table
	if expected, actual := -1, tbl.longestLen(); actual != expected {
		t.Errorf("Expected %d but was %d", expected, actual)
	} //if

	// Create some cells
	var a, b, c, d, e, f AlignLeft = "A", "B", "C", "D", "E", "F"

	// Create a table
	tbl = Table{
		rows: []*Row{
			&Row{cols: []Cell{&a}},
			&Row{cols: []Cell{&d, &e, &f}},
			&Row{cols: []Cell{&b, &c}},
		}, //slice
	} //struct

	// Check a table with different length rows
	if expected, actual := 3, tbl.longestLen(); actual != expected {
		t.Errorf("Expected %d but was %d", expected, actual)
	} //if
} //func

func TestTableWriteTo(t *testing.T) {
	// Create some cells
	var a, b, c, d AlignLeft = "A", "B", "C", "D"

	// Create a table
	tbl := Table{
		rows: []*Row{
			&Row{cols: []Cell{&a, &b}},
			&Row{cols: []Cell{&c, &d}},
		}, //slice
	} //struct

	// Create a buffer
	buff := bytes.NewBuffer(nil)

	// Write to the buffer
	_, err := tbl.WriteTo(buff)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check the string
	if expected, actual := "A B\nC D\n", buff.String(); actual != expected {
		t.Errorf("Expected\n\"%s\"but was\n\"%s\"", actual, expected)
	} //if
} //func

func TestTableString(t *testing.T) {
	// Create some cells
	var a, b, c, d AlignLeft = "A", "B", "C", "D"

	// Create a table
	tbl := Table{
		rows: []*Row{
			&Row{cols: []Cell{&a, &b}},
			&Row{cols: []Cell{&c, &d}},
		}, //slice
	} //struct

	// Check the string
	if expected, actual := "A B\nC D\n", tbl.String(); actual != expected {
		t.Errorf("Expected\n\"%s\"but was\n\"%s\"", actual, expected)
	} //if
} //func
